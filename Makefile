# Makefile


build:
	make stop
	docker rm qmobi_test
	docker build -t qmobi_test app
	docker run -d -p 8080:8080 --name qmobi_test qmobi_test

start:
	docker start qmobi_test

stop:
	docker stop qmobi_test

logs:
	docker logs -f qmobi_test

restart:
	make stop
	make start

update:
	make pull
	make build
