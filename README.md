### Requirements:
* docker

### Installation:
1. git clone https://gitlab.com/AlexKird/qmobi_test.git
2. cd qmobi_test
3. make build


### How to use:
#### 1. Get list of available currencies

*request:*
```
curl --location --request GET 'localhost:8080/currencies'
```

*response:* ```{"result": ["AUD",
        "BGN",
        "BRL",
        "CAD",
        "CHF",
        "CNY",
        "CZK",
        "DKK",
        "GBP",
        "HKD",
        "HRK",
        "HUF",
        "IDR",
        "ILS",
        "INR",
        "ISK",
        "JPY",
        "KRW",
        "MXN",
        "MYR",
        "NOK",
        "NZD",
        "PHP",
        "PLN",
        "RON",
        "RUB",
        "SEK",
        "SGD",
        "THB",
        "TRY",
        "USD",
        "ZAR"
    ]}```

#### 2. Convert currency

*request:*
```
curl --location --request POST 'localhost:8080/convert' \
--header 'Content-Type: application/json' \
--data-raw '{
    "amount": 5000,
    "income": "rub",
    "outcome": "usd"
}'
```

*response:* ```{
    "amount": 65.94284516855699,
    "code": "USD"
}```

