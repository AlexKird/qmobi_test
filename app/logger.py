import logging
import sys


def get_logger(name):
    formatter = logging.Formatter(
        '%(asctime)s - %(levelname)s - %(message)s')
    sh = logging.StreamHandler(sys.stdout)
    sh.setFormatter(formatter)
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(sh)
    return logger


main_logger = get_logger('main')
