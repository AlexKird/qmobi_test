from http.server import BaseHTTPRequestHandler, HTTPServer
from urllib.request import urlopen
import json
from logger import main_logger as logger


class Server(BaseHTTPRequestHandler):
    def _set_headers(self, status_code: int = 200):
        self.send_response(status_code)
        self.send_header('content-type', 'application/json')
        self.end_headers()

    def _send_json(self, body: dict, status_code: int = 200):
        self._set_headers(status_code)
        logger.info(f'send data: {body}')
        self.wfile.write(bytes(json.dumps(body, ensure_ascii=False), 'utf-8'))

    def _read_json(self) -> dict:
        content_len = int(self.headers.get('Content-Length'))
        data = json.loads(self.rfile.read(content_len))
        logger.info(f'receive data: {data}')
        return data

    def do_HEAD(self):
        self._set_headers()

    def do_GET(self):
        try:
            if self.path == '/currencies':
                self._send_json(self.currencies())
            else:
                self._send_json({'error': 'not found'}, status_code=404)
        except Exception as e:
            logger.exception(e)
            self._send_json({'error': str(type(e).__name__)}, status_code=500)

    def do_POST(self):
        try:
            if self.path == '/convert':
                data = self._read_json()
                self._send_json(self.convert(data))
            else:
                self._send_json({'error': 'not found'}, status_code=404)
        except Exception as e:
            logger.exception(e)
            self._send_json({'error': str(type(e).__name__)}, status_code=500)

    def log_message(self, format, *args):
        logger.info(format % args)

    @staticmethod
    def convert(data: dict) -> dict:
        income = Currency(
            amount=data['amount'],
            code=data['income'])
        outcome = income.convert_to(
            code=data['outcome'])
        return vars(outcome)

    @staticmethod
    def currencies() -> dict:
        currencies = Converter.get_available_currencies()
        return {'result': currencies}


class Currency:
    def __init__(self, amount: float, code: str = 'rub'):
        self.amount = amount
        self.code = code.upper()

    def convert_to(self, code: str = 'usd'):
        converter = Converter()
        return converter.convert(currency=self, code=code)

    def __str__(self):
        return f'Currency({self.amount}, {self.code})'


class Converter:
    def __init__(self):
        self.rates = self._get_rates()

    def convert(self, currency: Currency, code: str) -> Currency:
        logger.info(f'try to convert {currency} to {code}')
        income_rate = self.get_current_rate(currency.code)
        outcome_rate = self.get_current_rate(code)
        result = Currency(amount=currency.amount / income_rate * outcome_rate, code=code)
        logger.info(f'result: {result}')
        return result

    def get_current_rate(self, code: str) -> float:
        logger.info(f'getting current {code} rate')
        try:
            return self.rates[code.upper()]
        except KeyError as e:
            logger.exception(e)
            raise UnknownCurrencyCode

    @classmethod
    def _get_rates(cls) -> dict:
        logger.info(f'parse rates')
        try:
            rates_page = cls._make_request('https://www.iban.com/exchange-rates')
            rates = dict()
            for row in rates_page.split('<tbody>')[1].split('<tr>'):
                parsed_values = list()
                for col in row.split('<td>'):
                    value = col.split('</')[0].split('>')[-1].rstrip().lstrip()
                    parsed_values.append(value)
                if len(parsed_values) == 4:
                    rates[parsed_values[1]] = float(parsed_values[3])
            return rates
        except Exception as e:
            logger.exception(e)
            raise CurrencyParseError

    @staticmethod
    def _make_request(url: str):
        response = urlopen(url, timeout=5)
        return response.read().decode('utf-8')

    @classmethod
    def get_available_currencies(cls) -> list:
        return list(sorted(cls._get_rates().keys()))


class UnknownCurrencyCode(Exception):
    pass


class CurrencyParseError(Exception):
    pass


if __name__ == '__main__':
    httpd = HTTPServer(('0.0.0.0', 8080), Server)
    httpd.serve_forever()
