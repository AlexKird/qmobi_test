from main import Currency, Converter, UnknownCurrencyCode, Server
from http.server import HTTPServer
from urllib.request import urlopen, Request
from unittest import TestCase
from threading import Thread
import json


class ConverterTests(TestCase):
    def test_can_get_rates(self):
        rates = Converter._get_rates()
        self.assertEqual(type(rates), dict)
        self.assertGreater(len(rates), 0)

    def test_can_make_request(self):
        test_url = 'https://ya.ru'
        response = Converter._make_request(test_url)
        self.assertEqual(type(response), str)
        self.assertIn('Яндекс', response)

    def test_can_get_rub_rate(self):
        rub = Converter().get_current_rate('rub')
        self.assertGreater(rub, 0)
        self.assertEqual(type(rub), float)

    def test_can_convert(self):
        currency = Currency(200, 'rub')
        converter = Converter()
        result = converter.convert(currency, 'usd')
        self.assertTrue(type(result), Currency)
        self.assertGreater(currency.amount, result.amount)
        self.assertEqual(result.code, 'USD')
        self.assertGreater(result.amount, 0)

    def test_unknown_currency(self):
        currency = Currency(200, 'rub')
        converter = Converter()
        with self.assertRaises(UnknownCurrencyCode):
            result = converter.convert(currency, 'usdd')


class CurrencyTests(TestCase):
    def test_can_convert(self):
        currency = Currency(200, 'rub')
        result = currency.convert_to('usd')
        self.assertTrue(type(result), Currency)
        self.assertGreater(currency.amount, result.amount)
        self.assertEqual(result.code, 'USD')
        self.assertGreater(result.amount, 0)


class ServerTests(TestCase):
    httpd = HTTPServer(("localhost", 8080), Server)

    def setUp(self):
        thread = Thread(target=self.httpd.serve_forever)
        thread.start()

    def tearDown(self):
        self.httpd.shutdown()

    @staticmethod
    def get_request(url: str):
        response = urlopen(url, timeout=5)
        response_body = response.read()
        return json.loads(response_body.decode("utf-8"))

    @staticmethod
    def post_request(url: str, data: dict):
        req = Request(url, json.dumps(data).encode('utf-8'),
                      headers={'Content-Type': 'application/json'})
        resp = urlopen(req)
        return json.loads(resp.read().decode("utf-8"))

    def test_can_get_currencies(self):
        result = self.get_request('http://localhost:8080/currencies')
        self.assertIn('result', result)
        self.assertEqual(type(result['result']), list)

    def test_can_convert_usd_to_rub(self):
        data = {
            "amount": 200,
            "income": "usd",
            "outcome": "rub"
        }
        url = 'http://localhost:8080/convert'
        result = self.post_request(url, data)
        self.assertIn('amount', result)
        self.assertIn('code', result)
        self.assertEqual(result['code'], 'RUB')

    def test_can_convert_rub_tu_usd(self):
        data = {
            "amount": 200,
            "income": "rub",
            "outcome": "usd"
        }
        url = 'http://localhost:8080/convert'
        result = self.post_request(url, data)
        self.assertIn('amount', result)
        self.assertIn('code', result)
        self.assertEqual(result['code'], 'USD')

